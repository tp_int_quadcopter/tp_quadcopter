#include <TimerThree.h>
#include <Servo.h>

//Variable Declare
unsigned long recTimeout = 2000; //timeout from receiver
unsigned long trigTimeout = 16000; //timeout for trig
unsigned long pingRate = 200000; //Ping Rate in µS

Servo AIL; //Output for AILERON
Servo ELE; //Output for ELEVATOR
Servo THR; //Output for THROTTLE
Servo RUD; //Output for RUDDER
/* Output declared as servo
** to make use of PWM functions */

unsigned long ailVal; //Pulse duration of ailIn
unsigned long eleVal; //Pulse duration of eleIn
unsigned long thrVal; //Pulse duration of thrIn
unsigned long rudVal; //Pulse duration of rudIn
unsigned long auxVal; //Pulse duration of auxIn

volatile double distF; //Distance in Front
volatile double distB; //Distance behind
volatile double distL; //Distance on Left
volatile double distR; //Distance on Right
volatile double altitude; //Another word for distZ, Distance below
/* Volatile Modifier to prevent Microcontroller
** optimising variable. "Allows" variable to be changed by ISR. */

boolean LED_STATE;
unsigned long trig_time;
float trig_dist;
//Variable Declare End

//Pin Assignments
const int LED = 13;

const int trigF = 22; //trig pin for Sonar facing Front
const int trigR = 24; //Right
const int trigZ = 26; //Down
const int trigL = 28; //Left

const int echoF = 23; //Echo pin for Sonar facing Front
const int echoR = 25; //Right
const int echoZ = 27; //Down
const int echoL = 29; //Left

const int ailOut = 45; //Aileron out to controller
const int eleOut = 47; //Elevator out
const int thrOut = 49; //Throttle out
const int rudOut = 51; //Rudder Out

const int ailIn = 38; //Aileron in from receiver
const int eleIn = 40; //Elevator in
const int thrIn = 42; //Throttle in
const int rudIn = 44; //Rudder In
const int auxIn = 52; //Auxilliary In
//Pin Assignment End

void setup() {
 noInterrupts(); //Disable all interrupts
 
 Timer3.initialize(pingRate); //Init Timer3 for Sonar pingRate
 Timer3.attachInterrupt(readSonar); //Run readSensor as ISR
 
 Serial.begin(9600); //Start Serial monitor
 
 pinMode(LED, OUTPUT); //LED as Output
 
 pinMode(trigZ, OUTPUT); //Ultrasonic trig as output
 pinMode(trigF, OUTPUT);
 pinMode(trigL, OUTPUT);
 pinMode(trigR, OUTPUT);
 
 pinMode(echoZ, INPUT); //Ultrasonic echo as input
 pinMode(echoF, INPUT);
 pinMode(echoL, INPUT);
 pinMode(echoR, INPUT);
 
 pinMode(ailOut, OUTPUT); //Output to flight controller
 pinMode(eleOut, OUTPUT);
 pinMode(thrOut, OUTPUT);
 pinMode(rudOut, OUTPUT);
 
 pinMode(ailIn, INPUT); //Input from receiver
 pinMode(eleIn, INPUT);
 pinMode(thrIn, INPUT);
 pinMode(rudIn, INPUT);
 pinMode(auxIn, INPUT);
 
 AIL.attach(ailOut); //Attach "servos"
 ELE.attach(eleOut);
 THR.attach(thrOut);
 RUD.attach(rudOut);
 
 interrupts(); //enable all interrupts
}

void loop() {
 //Read Receiver inputs
 ailVal = pulseIn(ailIn, HIGH, recTimeout); //Read aileron
 eleVal = pulseIn(eleIn, HIGH, recTimeout); //Read elevator
 thrVal = pulseIn(thrIn, HIGH, recTimeout); //Read throttle
 rudVal = pulseIn(rudIn, HIGH, recTimeout); //Read Rudder
 auxVal = pulseIn(auxIn, HIGH, recTimeout); //Read Auxiliary
 
// Serial.print("AIL: ");
// Serial.print(ailVal);
// Serial.print("\t");
// Serial.print(" ELE: ");
// Serial.print(eleVal);
// Serial.print("\t");
// Serial.print(" THR: ");
// Serial.print(thrVal);
// Serial.print("\t");
// Serial.print(" RUD: ");
// Serial.print(rudVal);
// Serial.print("\t");
// Serial.print(" AUX: ");
// Serial.print(auxVal);
// Serial.println("");
 
}

void readSonar() {
  digitalWrite(LED, HIGH); //Start readSonar
  pingFront(); //Get space in front
  pingRight(); //Get space at right
  pingDown(); //Get space below
  pingLeft(); //Get space at left
//  Serial.print(distF);
//  Serial.print("\t");
//  Serial.print(distR);
//  Serial.print("\t");
//  Serial.print(distL);
//  Serial.print("\t");
//  Serial.println(altitude);
  
  digitalWrite(LED, LOW);
}

void pingDown() {
  digitalWrite(trigZ, LOW); //Ensure clean Pulse
  delayMicroseconds(2);
  digitalWrite(trigZ, HIGH); //Pulse for 10 microsecond
  delayMicroseconds(10);
  digitalWrite(trigZ, LOW); //Turn off again
  trig_time = pulseIn(echoZ, HIGH, trigTimeout); //Read pulse width
  altitude = calDist(trig_time); //Calculate Distance from time
}

void pingFront() {
  digitalWrite(trigF, LOW); //Ensure clean Pulse
  delayMicroseconds(2);
  digitalWrite(trigF, HIGH); //Pulse for 10 microsecond
  delayMicroseconds(10);
  digitalWrite(trigF, LOW); //Turn off again
  trig_time = pulseIn(echoF, HIGH, trigTimeout); //Read pulse width
  distF = calDist(trig_time); //Calculate Distance from time
}

void pingRight() {
  digitalWrite(trigR, LOW); //Ensure clean Pulse
  delayMicroseconds(2);
  digitalWrite(trigR, HIGH); //Pulse for 10 microsecond
  delayMicroseconds(10);
  digitalWrite(trigR, LOW); //Turn off again
  trig_time = pulseIn(echoR, HIGH, trigTimeout); //Read pulse width
  distR = calDist(trig_time); //Calculate Distance from time
}

void pingLeft() {
  digitalWrite(trigL, LOW); //Ensure clean Pulse
  delayMicroseconds(2);
  digitalWrite(trigL, HIGH); //Pulse for 10 microsecond
  delayMicroseconds(10);
  digitalWrite(trigL, LOW); //Turn off again
  trig_time = pulseIn(echoL, HIGH, trigTimeout); //Read pulse width
  distL = calDist(trig_time); //Calculate Distance from time
}

float calDist(int t) {
  trig_dist = (t/2) / 29.1; // t/2 for one trip
  return trig_dist;
}
