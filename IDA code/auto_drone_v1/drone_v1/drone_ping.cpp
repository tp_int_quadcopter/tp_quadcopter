
#include "drone_ping.h"

int  pingZ() {
  return check_distance(trigZ, echoZ);
}
int pingR() {
  return check_distance(trigR, echoR);
}
int pingL() {
  return check_distance(trigL, echoL);
}

int pingF() {
  return check_distance(trigF, echoF);
}

int pingB() {
  return check_distance(trigB, echoB);
}

unsigned long check_distance(int t, int e) {
  unsigned long duration, distance;
  digitalWrite(t, LOW);  
  delayMicroseconds(2); 
  digitalWrite(t, HIGH);
  delayMicroseconds(5); 
  digitalWrite(t, LOW);
  duration = pulseIn(e, HIGH);
  distance = (duration / 2) / 29.1;
  return (distance);

}
