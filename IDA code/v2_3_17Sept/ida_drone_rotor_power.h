
#ifndef IDA_DRONE_ROTOR
#define IDA_DRONE_ROTOR



/*

this file provides functionality to the quadcopter
to react how much power to give to the rotor based on 
proximity to the "wall"

*/



// returns how much power to give to the rotor based
// on distance supplied

// are we using imperial or metrics???
int react_to_wall(int );

#endif
