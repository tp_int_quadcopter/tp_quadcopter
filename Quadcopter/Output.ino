void SendFakePulse(){
  AIL.writeMicroseconds(1500); //Send Pulse
  delay(10);
  ELE.writeMicroseconds(1500); 
  delay(10);
  THR.writeMicroseconds(1100); 
  delay(10);
  RUD.writeMicroseconds(1500); 
  delay(10);
}

void OutputMaster() {
  AIL.writeMicroseconds(out_ail_val); //Write to AIL
  ELE.writeMicroseconds(out_ele_val); //Write to ELE
  THR.writeMicroseconds(out_thr_val); //Write to THR
  RUD.writeMicroseconds(out_rud_val); //Write to RUD
}
