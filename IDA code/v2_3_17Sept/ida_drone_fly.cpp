
#include "ida_drone_fly.h"
#include "ida_drone_ping.h"
#include "ida_drone_rotor_power.h"

#define DEBUG x

int fly_state = IDA_NEUTRAL;

void hello()
{
  //alvin was here
}



/**

seek: this is the drone bootstrap function. When it achieves auto pilot it will try
to look for a left wall.
Why? Because I'm left handed.


Seeks out a minimum of of one wall
After specific time_ticks of not finding any wall, it will descend.

*/

int seek(int dir)
{
  // check all around to see if we have sights
  
  //we prefer LEFT/FRONT
  
  //int r = pingR();
  //int b = pingB();
  //int f = pingF();
  int distance_seek = 0;
  switch(dir)
  {
   
    case IDA_LEFT:
        distance_seek = pingL();
        //#ifdef DEBUG
        //Serial1.print("left: ");
        Serial1.println(distance_seek);
        Serial1.println("cm");
        //#endif  
    
  
        break;
  
    case IDA_FRONT:   
        distance_seek = pingF();
        break;
    
    
   }
  
    
    
    if( distance_seek > WALL_TOLERANCE_MIN || distance_seek < SPOOKY_RANGE ) 
    {
      // SEEK WALL since I can't find it!
      return SEEKING_IN;
      
    }
    
    
     else if(distance_seek < TOO_CLOSE)
  {
    //jump away ASAP
    return AIELERON_MAX_R_SEEK_OUT;
  }
  else if(distance_seek > TOO_CLOSE && distance_seek <SOMEWHAT_CLOSE)
  {
    return AIELERON_MID_R_SEEK_OUT;
    // push away strongly
  }
  else if(distance_seek > SOMEWHAT_CLOSE && distance_seek < NOT_CLOSE)
  {
    return AIELERON_SLIGHT_R_SEEK_OUT;
    
    //gently push off
  }

    else
      //we're in range. Try to keep in view of it!
      {
       
      return SEEK_OPTIMAL;
      }
 
 
  return 0;
  //seek one direction and check sensors
  
  
  
}



