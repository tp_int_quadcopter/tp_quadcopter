
#include "ida_drone_ping.h"

int  pingZ() {
  return check_distance(trigZ, echoZ);
}
int pingR() {
  return check_distance(trigR, echoR);
}
int pingL() {
  return check_distance(trigL, echoL);
}

int pingF() {
  return check_distance(trigF, echoF);
}

int pingB() {
  return check_distance(trigB, echoB);
}



int check_distance(int t, int e) {
  long duration, distance;
  digitalWrite(t, LOW);  
  delayMicroseconds(2); 
  digitalWrite(t, HIGH);
  delayMicroseconds(10); 
  digitalWrite(t, LOW);
  duration = pulseIn(e, HIGH,12000);
  distance = (duration / 2) / 29.1;
  if(duration>15000)
    distance=  250;
    
  return (distance);
}



unsigned long xcheck_distance(int t, int e) {
  unsigned long duration, distance;
  digitalWrite(t, LOW);  
  delayMicroseconds(2); 
  digitalWrite(t, HIGH);
  delayMicroseconds(5); 
  digitalWrite(t, LOW);
  duration = pulseIn(e, HIGH, 15000);//last parameter is a timeout
  distance = (duration / 2) / 29.1;
  Serial1.print("Duration :");
  Serial1.println(duration);
  if(duration>15000)//duration represents the pinged distance
    distance=  250;
  
  return (distance);

 // pinMode(ultraSoundSignal, OUTPUT); // Switch signalpin to output
 // digitalWrite(ultraSoundSignal, LOW); // Send low pulse
 // delayMicroseconds(2); // Wait for 2 microseconds
 // digitalWrite(ultraSoundSignal, HIGH); // Send high pulse
 // delayMicroseconds(5); // Wait for 5 microseconds
 // digitalWrite(ultraSoundSignal, LOW); // Holdoff
 // pinMode(ultraSoundSignal, INPUT); // Switch signalpin to input
 // digitalWrite(ultraSoundSignal, HIGH); // Turn on pullup resistor
 //  // please note that pulseIn has a 1sec timeout, which may
 //  // not be desirable. Depending on your sensor specs, you
 //  // can likely bound the time like this -- marcmerlin
 //  // echo = pulseIn(ultraSoundSignal, HIGH, 38000)
 //  echo = pulseIn(ultraSoundSignal, HIGH); //Listen for echo
 //  ultrasoundValue = (echo / 58.138) * .39; //convert to CM then to inches
 // return ultrasoundValue;
}
