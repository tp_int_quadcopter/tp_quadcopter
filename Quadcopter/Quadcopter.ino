#include <TimerThree.h>
#include <TimerOne.h>
#include <Servo.h>
#include "Constants.h"

//#define DEBUG_REC 1 //to enable Receiver debug
//#define DEBUG_OUT 1 //to enable Output debug
#define DEBUG_PING 1 //to enable Ultrasonic debug

//Variable Declare
Servo AIL; //Output for AILERON
Servo ELE; //Output for ELEVATOR
Servo THR; //Output for THROTTLE
Servo RUD; //Output for RUDDER
/* Output declared as servo
** to make use of PWM functions */

unsigned int in_rec_ail; //Pulse duration of ailIn
unsigned int in_rec_ele; //Pulse duration of eleIn
unsigned int in_rec_thr; //Pulse duration of thrIn
unsigned int in_rec_rud; //Pulse duration of rudIn
unsigned int in_rec_aux; //Pulse duration of auxIn

unsigned int out_ail_val; //Ailerons output value
unsigned int out_ele_val; //Elevator output value
unsigned int out_thr_val; //Throttle output value
unsigned int out_rud_val; //Rudder output value

volatile float dist_F; //Front Distance
volatile float dist_R; //Right Distance
volatile float dist_Z_cm; //Down Distance
volatile float dist_L; //Left Distance
/* Volatile Modifier to prevent Microcontroller
** optimising variable. "Allows" variable to be changed by ISR. */

//Variable Declare End

void setup() {
 noInterrupts(); //Disable all interrupts
 
 Timer3.initialize(PING_RATE_US); //Init Timer3 for Sonar PING_RATE_US
 Timer3.attachInterrupt(TimerService); //Run TimerService as ISR
// Timer1.initialize(OUTPUT_RATE_US);
// Timer1.attachInterrupt(OutputMaster);
 
 Serial.begin(9600); //Start Serial monitor
 
 pinMode(PIN_LED, OUTPUT); //PIN_LED as Output
 
 pinMode(PIN_TRIGZ, OUTPUT); //Ultrasonic TRIG as output
 pinMode(PIN_TRIGF, OUTPUT);
 pinMode(PIN_TRIGL, OUTPUT);
 pinMode(PIN_TRIGR, OUTPUT);
 
 pinMode(PIN_ECHOZ, INPUT); //Ultrasonic ECHO as input
 pinMode(PIN_ECHOF, INPUT);
 pinMode(PIN_ECHOL, INPUT);
 pinMode(PIN_ECHOR, INPUT);
 
 pinMode(PIN_AIL_OUT, OUTPUT); //Output to flight controller
 pinMode(PIN_ELE_OUT, OUTPUT);
 pinMode(PIN_THR_OUT, OUTPUT);
 pinMode(PIN_RUD_OUT, OUTPUT);
 
 pinMode(PIN_AIL_IN, INPUT); //Input from receiver
 pinMode(PIN_ELE_IN, INPUT);
 pinMode(PIN_THR_IN, INPUT);
 pinMode(PIN_RUD_IN, INPUT);
 pinMode(PIN_AUX_IN, INPUT);
 
 AIL.attach(PIN_AIL_OUT); //Attach "servos"
 ELE.attach(PIN_ELE_OUT);
 THR.attach(PIN_THR_OUT);
 RUD.attach(PIN_RUD_OUT);
 
 SendFakePulse();
 /* Send a short pulse through each channel
 ** to establish handshake.*/
 out_thr_val = THR_MIN;
 interrupts(); //enable all interrupts
}

void loop() {

#ifdef DEBUG_REC
 Serial.print("AIL: ");
 Serial.print(in_rec_ail);
 Serial.print("\t");
 Serial.print(" ELE: ");
 Serial.print(in_rec_ele);
 Serial.print("\t");
 Serial.print(" THR: ");
 Serial.print(in_rec_thr);
 Serial.print("\t");
 Serial.print(" RUD: ");
 Serial.print(in_rec_rud);
 Serial.print("\t");
 Serial.print(" AUX: ");
 Serial.print(in_rec_aux);
 Serial.println("");
#endif

#ifdef DEBUG_OUT
 Serial.print("AIL: ");
 Serial.print(out_ail_val);
 Serial.print("\t");
 Serial.print(" ELE: ");
 Serial.print(out_ele_val);
 Serial.print("\t");
 Serial.print(" THR: ");
 Serial.print(out_thr_val);
 Serial.print("\t");
 Serial.print(" RUD: ");
 Serial.print(out_rud_val);
 Serial.println("");
 
#endif

#ifdef DEBUG_PING
//  Serial.print(dist_F);
//  Serial.print("\t");
//  Serial.print(dist_R);
//  Serial.print("\t");
//  Serial.print(dist_L);
//  Serial.print("\t");
#endif  
}

void TimerService() {
//  Serial.println("Timer Interrupt");
  digitalWrite(PIN_LED, HIGH); //Start readSonar
   //Read Receiver inputs
  ReadReceiver();
  if(IsOverride()) { //Check override switch
  //if(0){
    FlyManual(); //Manual Takeover
  }
  else {
    GetPosition(); //Get position
    DoAutoPilot(); //Do Autonomous Routine
  }
  OutputMaster(); //Remove after test.

    digitalWrite(PIN_LED, LOW); //Off LED

}

