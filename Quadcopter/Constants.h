//Pin Assignments
#define PIN_LED 13
#define PIN_TRIGF 22 //TRIG pin for Sonar facing Front
#define PIN_TRIGR 24 //Right
#define PIN_TRIGZ 26 //Down
#define PIN_TRIGL 28 //Left

#define PIN_ECHOF 23 //Echo pin for Sonar facing Front
#define PIN_ECHOR 25 //Right
#define PIN_ECHOZ 27 //Down
#define PIN_ECHOL 29 //Left

#define PIN_AIL_OUT 39 //Aileron out to controller
#define PIN_ELE_OUT 41 //Elevator out
#define PIN_THR_OUT 43 //Throttle out
#define PIN_RUD_OUT 45 //Rudder Out

#define PIN_AIL_IN 38 //Aileron in from receiver
#define PIN_ELE_IN 40 //Elevator in
#define PIN_THR_IN 42 //Throttle in
#define PIN_RUD_IN 44 //Rudder In
#define PIN_AUX_IN 50 //Auxilliary In
//Pin Assignment End



//Other Constants
#define REC_TIMEOUT_US 20000 //timeout from receiver in µS
#define TRIG_TIMEOUT_US 16000 //timeout for TRIG in µS
#define PING_RATE_US 200000 //Ping Rate in µS
#define OUTPUT_RATE_US 100000 //Output rate in µS
#define HOVER_HEIGHT_CM 40 //Hover Height in cm
#define HOVER_ALLOWANCE_CM 10 //Allowance in cm
#define HOVER_HEIGHT_TOLERANCE            0.2 // Tolerance in percentage for hoverheight
#define ADJ_DECR_THR_VALUE               10
#define ADJ_INCR_THR_VALUE               5

#define ALT_HOLD_MULTIPLIER 1 //Altitude hold multiplier, too sensitive decrease value
#define SOFT_LANDING_MULTIPLIER 1 //Soft Landing multiplier, too sensitive increase value
#define THR_MIN 1200 // Min of THR
#define THR_THRESHOLD 1300 //Midpoint of THR
#define THR_MAX 1850 //Max of THR
#define AUX1_THRESHOLD 1400 //Midpoint of AUX1
#define OUT_OF_RANGE_CM 99999.0 //

#define STATE_DEC_THR                  0
#define STATE_INC_THR                  1
#define STATE_HOLD_THR                 2


#define Z_CHANGE_POS_CM              1.5
#define Z_CHANGE_NEG_CM              1.5
#define Z_PREV_INITAL                8.5
//Other Constants End
