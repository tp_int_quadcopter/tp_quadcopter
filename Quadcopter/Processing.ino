unsigned int throttle_state = STATE_HOLD_THR;
float prev_Z_cm = 0.0;


void DoAutoPilot() //Function 1
{
  HoldAltitude();
}

void FlyManual() { //Function 2
  //Values passthrough
  out_ail_val = in_rec_ail; //Relay Information
  out_ele_val = in_rec_ele;
  out_thr_val = in_rec_thr;
  out_rud_val = in_rec_rud;
}

void TakeOff() { //Function 3
  for(int i = 1000; i < 1500; i++) {
    out_thr_val = i;
    delay(100);
    if(dist_Z_cm > HOVER_HEIGHT_CM) {
      break;
    }
  }
}

/*void HoldAltitude() { //Function 4
  float height_diff;//Difference between curr. height and desired height
  float thr_compensate; //Throttle Compensate

  height_diff = HOVER_HEIGHT_CM - dist_Z_cm;//Cal height_diff
  thr_compensate = (height_diff*ALT_HOLD_MULTIPLIER);//Cal throttle to compensate
  out_thr_val = out_thr_val + thr_compensate;//Compensate thr_val
  out_thr_val = min(out_thr_val, THR_MAX); //set max val for thr
  out_thr_val = max(out_thr_val, THR_MIN); //set min val for thr
  Serial.println(out_thr_val);
}
*/

/*---------------------------------------------------------------------------------------------
  Function name : HoldAltitude()
  Description   : 
  Return value  : 
---------------------------------------------------------------------------------------------*/
void HoldAltitude() { //Function 3

  switch(throttle_state){
    case STATE_HOLD_THR:{
      Serial.println("STATE_HOLD_THR");
 //     Serial.print("prev_Z_cm : ");
 //     Serial.println(prev_Z_cm);
//      Serial.print("dist_Z_cm : ");
//      Serial.println(dist_Z_cm);
      if(dist_Z_cm < (1.0-HOVER_HEIGHT_TOLERANCE)*HOVER_HEIGHT_CM)
      {
        // If altitude is below hover height tolerance, go to STATE_INC_THR
        throttle_state = STATE_INC_THR;
      }
      else if(dist_Z_cm > (1.0+HOVER_HEIGHT_TOLERANCE)*HOVER_HEIGHT_CM)
      {
        // else if altitude is above hover height tolerance go to STATE_DEC_THR
        throttle_state = STATE_DEC_THR;
      }
      // else do nothing, hover altitude within tolerance, let throttle value stay unchanged
      break;
    }
    case STATE_INC_THR:{
      
      Serial.println("STATE_INC_THR");
//      Serial.print("prev_Z_cm : ");
//      Serial.println(prev_Z_cm);
//      Serial.print("dist_Z_cm : ");
//      Serial.println(dist_Z_cm);
      
      if((dist_Z_cm - prev_Z_cm) > Z_CHANGE_POS_CM)
      {
        // If the rate in rise in altitude is acceptable, then go back to STATE_HOLD_THR
        throttle_state = STATE_HOLD_THR;
      }
      else if(dist_Z_cm > (1.0+HOVER_HEIGHT_TOLERANCE)*HOVER_HEIGHT_CM) 
      {
        // If the altitude has risen above tolerance, then go to STATE_DEC_THR
        throttle_state = STATE_DEC_THR;
      }
      else
      {
          // increment the throttle
          if((out_thr_val + ADJ_INCR_THR_VALUE) < THR_MAX)
          {
            out_thr_val += ADJ_INCR_THR_VALUE;
          }
      }
      break;
    }
    case STATE_DEC_THR:{
      Serial.println("STATE_DEC_THR");
  //    Serial.print("prev_Z_cm : ");
  //    Serial.println(prev_Z_cm);
  //    Serial.print("dist_Z_cm : ");
  //    Serial.println(dist_Z_cm);
      if(dist_Z_cm < (prev_Z_cm - Z_CHANGE_NEG_CM))
      {
        // If altitude drop rate is acceptable, go to STATE_HOLD_THR
        throttle_state = STATE_HOLD_THR;
      }
      else if(dist_Z_cm < (1.0-HOVER_HEIGHT_TOLERANCE)*HOVER_HEIGHT_CM) 
      {
        // else if the altitude has dropped below tolerance, go to STATE_INC_THR
        throttle_state = STATE_INC_THR;
      }
      else
      {
        if((out_thr_val - ADJ_DECR_THR_VALUE) > THR_MIN)
        {
          // decrement throttle
          out_thr_val -= ADJ_DECR_THR_VALUE;
        }
      }
      break;
    }
    default:{
//      Serial.println("default state");
      // should never get here ! go to STATE_HOLD_THR
      throttle_state = STATE_HOLD_THR;
      break;
    }
    
  }
  prev_Z_cm = dist_Z_cm; // remember the current altitude
  Serial.print("Thr :");
  Serial.println(out_thr_val);
}
void LandSoft() { //Function 5
  out_thr_val = (dist_Z_cm*SOFT_LANDING_MULTIPLIER) + THR_MIN; //Cal soft landing speed
  out_thr_val = min(out_thr_val, THR_THRESHOLD); //Set min to THR_THRESHOLD to guarantee alt drop
}

boolean IsOverride() {
  boolean override = false;

  if(in_rec_aux < AUX1_THRESHOLD) {// Aux Flipped Down 
    override = true;
  }
  return(override);
}
