
#include <Servo.h>

#include "ida_drone_fly.h"
#include "ida_drone_ping.h"
#define LED 13

#define STICK_DISTANCE 70 //was 30

#define HANG_DURATION 4

#define leftLED 44
#define rightLED 46

//#define DEBUG x

#define BLUETOOTH_DEBUG bt


/**
 * CHANGELOG:
 * Stripping out XY Code
 * Just focusing on getting Z correct
 * 
 * Have some suspicions that the "pendulum swings" from the craft
 * might angle the bottom ultra sound sensor and could potentially report
 * false readings
 * 
 * date 18th sept:
 *  tweaked and fixed Z response. reduce ceiling height from 30 to 20.
 * 
 * current code is working for FUTABA - weight and power ratio is perfect 
 *
 * Tweaking micro controller to always supply Aileron with a value every loop 
 * 
 */

Servo AIL;
Servo ELE;
Servo THR;
Servo RUD;
Servo AUX;

double InputZ, InputR, InputL, hoverHeight, latDiff, ratio, error;
int recAIL;
int recELE;
int recTHR;
int recRUD;
int recAUX;

int max_rec;
int min_rec;
int mid_rec;
int ref_aux;
int thr, ail;

unsigned long x_prev_time;
unsigned long y_prev_time;
unsigned long x_curr_time;
unsigned long x_elap_time;
unsigned long y_elap_time;
unsigned long y_curr_time;



short fly_dir = IDA_NEUTRAL;

#define TAKEOFF 0
#define CRUISE 1
#define GROUND 5
int throttle_flag=0;
int takeoff_flag=GROUND;



int fly_x_time_tick=0;
int fly_x_state= SEEK_OPTIMAL;

short  time_to_check=1;

void setup(){

  Serial.begin(9600);
  Serial1.begin(9600);
  Serial1.println("BT Connected");

  pinMode(trigZ, OUTPUT);
  pinMode(echoZ, INPUT);
  pinMode(trigR, OUTPUT);
  pinMode(echoR, INPUT);
  pinMode(trigL, OUTPUT);
  pinMode(echoL, INPUT);
  pinMode(trigF, OUTPUT);
  pinMode(echoF, INPUT);
  pinMode(trigB, OUTPUT);
  pinMode(echoB, INPUT);

  pinMode(leftLED, OUTPUT);
  pinMode(rightLED, OUTPUT);
  // pinMode(6, OUTPUT);
  // pinMode(9, OUTPUT);
  // pinMode(10, OUTPUT);
  // pinMode(11, OUTPUT);
  // pinMode(13, OUTPUT);
  // AIL.attach(6);
  // ELE.attach(9);
  // THR.attach(10);
  // RUD.attach(11);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  AIL.attach(9);
  ELE.attach(10);
  THR.attach(11);
  RUD.attach(12);


  //calibration loop
  for (int i = 0; i < 10; i++) {
    recAIL = pulseIn(4, HIGH, 20000);
    mid_rec += recAIL;
  }
  ref_aux = pulseIn(7, HIGH, 20000);
  mid_rec = mid_rec / 10;
  min_rec = mid_rec - 450;
  max_rec = mid_rec + 450;
  Serial.print("AIL AVG VAL: ");
  Serial.println(mid_rec);
  Serial.print("AUX AVG VAL: ");
  Serial.println(ref_aux);
  x_prev_time = millis();
  delay(500);

  hoverHeight=60;

  fakePulse();
  delay(500);
  //arm();


  Serial.print("Thrust at start");
  Serial.print("\t");
  Serial.println(thr);
}



/**********************************************
* void loop()
*********************************************/


void loop()
{
  
  
  
  //check for manual
  
  recAIL = pulseIn(3, HIGH, 20000);
  recELE = pulseIn(4, HIGH, 20000);
  recTHR = pulseIn(5, HIGH, 20000);
  recRUD = pulseIn(6, HIGH, 20000);
  recAUX = pulseIn(7, HIGH, 20000);

 if(recAUX > 0 && recAUX > (ref_aux + 400)  ){ //this is for the debugging drone paired with Futaba
    digitalWrite(LED,HIGH);
    autopilot_sensors();
    check_manual();
    THR.writeMicroseconds(thr);
    AIL.writeMicroseconds(ail);
    ELE.writeMicroseconds(recELE);  //to do
    RUD.writeMicroseconds(recRUD);  //to do
    
    Serial1.print("thr :");
    Serial1.println(thr);    
 }
  else
  {
    
    digitalWrite(LED,LOW);
    THR.writeMicroseconds(recTHR);
    AIL.writeMicroseconds(recAIL);
    ELE.writeMicroseconds(recELE);
    RUD.writeMicroseconds(recRUD);   

  }
  

  
  
}

/********************************************
* void check_manual
* function checks if pilot has overriden 
* the controls and will replace computed values 
* if it exists
********************************************/


void check_manual()
{
  if(recTHR > min_rec + STICK_DISTANCE  ){

    if(recTHR<1700)
      thr = recTHR;

    #ifdef DEBUG
    Serial.print("Throttle override: ");
    Serial.println(recTHR);
    #endif

  }
  if (recAIL < (mid_rec - STICK_DISTANCE) || recAIL > (mid_rec + STICK_DISTANCE)) {
    ail = recAIL;
    #ifdef DEBUG 
    Serial.print("AIL overridex: ");
    Serial.println(recAIL);
    #endif 
  }
}


/************************************
*
* void  autopilot_sensors()


This function will check the sensors to determine if the height and left-ness is
ok and tell the onboard flight controller to make some decisions

**************************************/



void autopilot_sensors()
{
  
  //if time is up we will take a sample of left sensor
  //always check Z because we're frelled if we don't. Thropter will just fall out of the sky
  InputZ = pingZ();
  
    if( InputZ < 5 && throttle_flag>1 )
    {
      #ifdef DEBUG
      Serial.print("Thrust at start");
      #endif
      throttle_flag= 0;
    }

    if(InputZ == 0)
    {
      // for detection of flight beyond safety high,
      // currently more than 2M, gently cut power and set it down.
      if(thr> min_rec)
      {
        thr-=6;
      }
    }

    else if( InputZ > 180){
      thr = min_rec;
    }
    else if(  InputZ < (hoverHeight - (hoverHeight * 0.30))   && throttle_flag==0 ){ //below hoverHeight
      error = ((hoverHeight - InputZ)/hoverHeight) * 100;
      thr = map(error, 0, 100, mid_rec, (mid_rec + 100));//used to be 200 instead of 300 
    }


    else if(  InputZ < (hoverHeight - (hoverHeight * 0.30))   && throttle_flag==1){ //below hoverHeight
      #ifdef BLUETOOTH_DEBUG
      Serial1.println("Outer :::::::: Easing back up");
      #endif
      if(thr< mid_rec)//to review whether to add more to the mid_rec
      {
        thr +=3;
        #ifdef DEBUG
        Serial.println("Easing back up");
        #endif
      }
    }
    //cut power when bot hits above hoverheight + distance,
    else if( ( InputZ > hoverHeight + (hoverHeight * 0.30) )   ){ // above hoverHeight
      if(thr> min_rec)
      {
        thr-= 6; 
        throttle_flag=1;
        Serial1.println("forcing down");
      }
    }
    // if not keep to 50% throttle 
    else if( InputZ > hoverHeight - (hoverHeight * 0.30) && InputZ < hoverHeight + (hoverHeight * 0.30) ) {
      //within hoverHeight
      thr = mid_rec;
    }  
    
    
    //check Left
    
    
    x_curr_time = millis();
    x_elap_time = x_curr_time - x_prev_time;    

    if ( x_elap_time > 100) {
              
                if(time_to_check>0)
                {
                  int seek_val = seek(IDA_LEFT);
                  
                  if( seek_val == SEEKING_IN)
                  {
                    //ail = mid_rec - 200; natural
                    ail = mid_rec - POWER_L_REG;
                    #ifdef BLUETOOTH_DEBUG
                    Serial1.println("Triggered: L");
                    #endif
                    //flag out scripted move to swing
                    //set flag to fly left
                    
                    time_to_check = HANG_DURATION;//fly left for 1s
                    fly_dir= IDA_LEFT;
                    ail = mid_rec - POWER_L_REG;
                  }
                  else if (seek_val == AIELERON_MAX_R_SEEK_OUT || seek_val == AIELERON_MID_R_SEEK_OUT || seek_val == AIELERON_SLIGHT_R_SEEK_OUT)
                  {
                    //flag out scripted move to swing
                    //set flag to fly left
                    time_to_check = HANG_DURATION;//fly left for 1s
                    fly_dir = IDA_RIGHT;
                    ail = mid_rec + POWER_R_MAX;
                  }
                  else
                  {
                    //safe zone.
                    ail = mid_rec;
                   time_to_check=100;
                  }
                }
                else
                {
                  //fly_dir= IDA_NEUTRAL;
                  time_to_check -=1;
                }
  }

}

void old_loop(){

  // recAIL = pulseIn(4,HIGH,20000);
  // recELE = pulseIn(5,HIGH,20000);
  // recTHR = pulseIn(7,HIGH,20000);
  // recRUD = pulseIn(8,HIGH,20000);
  // recAUX = pulseIn(12,HIGH,20000);
  //Serial.println(recAUX);
  recAIL = pulseIn(3, HIGH, 20000);
  recELE = pulseIn(4, HIGH, 20000);
  recTHR = pulseIn(5, HIGH, 20000);
  recRUD = pulseIn(6, HIGH, 20000);
  recAUX = pulseIn(7, HIGH, 20000);



  //test_LED();

  //if(recAUX > 0 && recAUX > (ref_aux + 400)  ){// this is for the FLYSKY FORCE 21 PAIRED WITH METAL FRAME
  //if(  recAUX > 0 && recAUX < (ref_aux - 200)  ){ //this is for the debugging drone paired with DX6
  if(recAUX > 0 && recAUX > (ref_aux + 400)  ){ //this is for the debugging drone paired with Futaba

    digitalWrite(LED,HIGH);
    /*
    Serial.print("Thrust before autopilot");
     Serial.print("\t");
     Serial.println(thr);
     */
    autoPilot();
    THR.writeMicroseconds(recTHR);
    AIL.writeMicroseconds(ail);
    ELE.writeMicroseconds(recELE);
    RUD.writeMicroseconds(recRUD);   
    
    /*
    Serial.print("Thrust after autopilot");
     Serial.print("\t");
     Serial.println(thr);
     */
  }
  else{
    digitalWrite(LED, LOW);
    THR.writeMicroseconds(recTHR);
    AIL.writeMicroseconds(recAIL);
  ELE.writeMicroseconds(recELE);
  RUD.writeMicroseconds(recRUD);



  }


  // print_receiver();
  // digitalWrite(rightLED, HIGH);
  //print_sensors();
  //Serial.println(recAUX);
  // flight_x();
}

void fakePulse(){

  AIL.writeMicroseconds(1500); 
  delay(10);
  ELE.writeMicroseconds(1500); 
  delay(10);
  THR.writeMicroseconds(1100); 
  delay(10);
  RUD.writeMicroseconds(1500); 
  delay(10);


}


void autoPilot(){
  //flight_z();
  InputZ = pingZ();
  flight_z();
  //flight_x();


  // TODO: Remove after debug
  fly_x();
  //return;
  //-- End TODO

  if( InputZ > hoverHeight - (hoverHeight * 0.20) && InputZ < hoverHeight + (hoverHeight * 0.20) ) { //within hoverHeight

    // fly x gets called only at the correct height zone. This is funky because if it's out of range flyx never gets called
    // potentially if flyer is tilted at an akward angle and it falls below or rises above the Z range then the copter 
    // is tilted uncontrollably.
    // this is very bad


    //flight_x(); 
    fly_x(); 

#ifdef DEBUG
    Serial.println("engaged autopilot");
#endif
  }
  else{
#ifdef DEBUG
    Serial.print("trying to achieve height, now at: ");
    Serial.println(InputZ);
#endif
    AIL.writeMicroseconds(recAIL);
  }




}

void flight_z(){


  // we should really put the aileron to mid to straighten out ? or send a stream of jobs to alternatively level and lower?
  // the rate of change is really brutal right now
  // no finess. Need to improve.


  if(recTHR > min_rec + STICK_DISTANCE  ){
    digitalWrite(LED,LOW);
    if(recTHR<1700)
      THR.writeMicroseconds(recTHR);

#ifdef DEBUG
    Serial.print("Throttle override: ");
    Serial.println(recTHR);
#endif

  }

  else{

    // InputZ = pingZ();

    // if( InputZ == 0 ){
    //   thr = min_rec;
    // }

    // }



    if( InputZ < 5 && throttle_flag>1 )
    {
      Serial.print("Thrust at start");
      throttle_flag= 0;

    }

    if(InputZ == 0)
    {
      // for detection of flight beyond safety high,
      // currently more than 2M, gently cut power and set it down.

      if(thr> min_rec)
      {
        thr-=6;
      }
    }

    else if( InputZ > 180){
      thr = min_rec;
    }


    else if(  InputZ < (hoverHeight - (hoverHeight * 0.30))   && throttle_flag==0 ){ //below hoverHeight
      error = ((hoverHeight - InputZ)/hoverHeight) * 100;
      thr = map(error, 0, 100, mid_rec, (mid_rec + 100));//used to be 200 instead of 300 
    }


    else if(  InputZ < (hoverHeight - (hoverHeight * 0.30))   && throttle_flag==1){ //below hoverHeight
      Serial1.println("Outer :::::::: Easing back up");
      if(thr< mid_rec)//to review whether to add more to the mid_rec
      {
        thr +=3;
        Serial.println("Easing back up");
      }

    }
    //cut power when bot hits above hoverheight + distance,
    else if( ( InputZ > hoverHeight + (hoverHeight * 0.30) )   ){ // above hoverHeight

      if(thr> min_rec)
      {
        thr-= 6; 
        throttle_flag=1;
        Serial1.println("forcing down");
      }


    }
    // if not keep to 50% throttle 
    else if( InputZ > hoverHeight - (hoverHeight * 0.30) && InputZ < hoverHeight + (hoverHeight * 0.30) ) {

      //within hoverHeight

      thr = mid_rec;

    }

    THR.writeMicroseconds(thr);
    /*
    Serial.print(InputZ);
     Serial.print("\t");
     Serial.println(thr);
     */

  }

}






void flight_y(){

  if (recELE < (mid_rec - 30) || recELE > (mid_rec + 30)) {
    digitalWrite(LED, LOW);
    //    Serial.println("AIL OVERWRITE");
    ELE.writeMicroseconds(recELE);
  }
  else {
    ELE.writeMicroseconds(mid_rec);
  }

}

void flight_w(){

  if (recRUD < (mid_rec - 30) || recRUD > (mid_rec + 30)) {
    digitalWrite(LED, LOW);
    //    Serial.println("AIL OVERWRITE");
    RUD.writeMicroseconds(recRUD);
  }
  else {
    RUD.writeMicroseconds(mid_rec);
  }

}



void print_receiver(){
  Serial.print("AIL: ");
  Serial.print(recAIL);
  Serial.print('\t');
  Serial.print("ELE: ");
  Serial.print(recELE);
  Serial.print('\t');
  Serial.print("THR: ");
  Serial.print(recTHR);
  Serial.print('\t');
  Serial.print("RUD: ");
  Serial.print(recRUD);
  Serial.print('\t');
  Serial.print("AUX: ");
  Serial.print(recAUX);
  Serial.print('\n');
}

void print_x_distance(){
  Serial1.print(InputL);
  Serial1.print("\t");
  Serial1.print(InputR);
  Serial1.print("\n");
}

void print_sensors(){
  // Serial.print("Z: ");
  // Serial.print(pingZ());
  // Serial.print("\t");

  Serial.print("L: ");
  Serial.print(pingL());
  Serial.print("\t");

  Serial.print("R: ");
  Serial.print(pingR());
  Serial.print("\t");

  // Serial.print("F: ");
  // Serial.print(pingF());
  // Serial.print("\t");

  // Serial.print("B: ");
  // Serial.print(pingB());
  // Serial.print("\t");

  Serial.print("\n");
}



void test_LED()

{

  digitalWrite(leftLED, HIGH);
  digitalWrite(rightLED, HIGH);
  delay(1000);
  digitalWrite(leftLED, LOW);
  digitalWrite(rightLED, LOW);

}

void fly_x()
{

  //TODO : OPTIMIZE

  if (recAIL < (mid_rec - STICK_DISTANCE) || recAIL > (mid_rec + STICK_DISTANCE)) {
    digitalWrite(LED, LOW);
    //    Serial.println("AIL OVERWRITE");

    AIL.writeMicroseconds(recAIL);
#ifdef DEBUG 
    Serial.print("AIL overridex: ");
    Serial.println(recAIL);
#endif 
  }
  else {




    x_curr_time = millis();
    x_elap_time = x_curr_time - x_prev_time;    

    if ( x_elap_time > 100) {


      //Correct if previous value is different from mid_rec

      //AIL.writeMicroseconds(mid_rec);
      digitalWrite(leftLED, LOW);
      digitalWrite(rightLED, LOW);




      x_prev_time= x_curr_time;


      int seek_val = seek(IDA_LEFT);

      if( seek_val == SEEKING_IN)
      {

        digitalWrite(leftLED, HIGH);
        //ail = mid_rec - 200; natural
        ail = mid_rec - POWER_L_REG;
        Serial1.println("Triggered: L");
        //AIL.writeMicroseconds(ail);


#ifdef BLUETOOTH_DEBUG
        Serial1.print("Steer Left, AIL is:");
        Serial1.println(ail);
#endif

#ifdef DEBUG
        Serial.print("Steer Left, AIL is:");
        Serial.println(ail);
#endif



      }
      else if(seek_val == AIELERON_MAX_R_SEEK_OUT)
      {
        //figure the distance to decide how much juice to give to the 
        //rotors to push it away from the wall



        digitalWrite(rightLED, HIGH);
        //ail = mid_rec + 200; natual
        ail = mid_rec + POWER_R_MAX;

        //AIL.writeMicroseconds(ail);
#ifdef BLUETOOTH_DEBUG
        Serial1.println("Triggered: R  ");
        Serial1.println(ail);


#endif
#ifdef DEBUG
        Serial.println("Triggered: R ");
        Serial1.println(ail);
#endif

      }
      else if(seek_val == AIELERON_MID_R_SEEK_OUT)
      {
        //figure the distance to decide how much juice to give to the 
        //rotors to push it away from the wall



        digitalWrite(rightLED, HIGH);
        //ail = mid_rec + 200; natual
        ail = mid_rec + POWER_R_MID;

        //AIL.writeMicroseconds(ail);
#ifdef BLUETOOTH_DEBUG
        Serial1.println("Triggered: R ");
        Serial1.println(ail);

#endif
#ifdef DEBUG
        Serial.println("Triggered: R ");
        Serial1.println(ail);
        ;
#endif


      }      
      else if(seek_val == AIELERON_SLIGHT_R_SEEK_OUT)
      {
        //figure the distance to decide how much juice to give to the 
        //rotors to push it away from the wall



        digitalWrite(rightLED, HIGH);
        //ail = mid_rec + 200; natual
        ail = mid_rec + POWER_R_LOW;

        //AIL.writeMicroseconds(ail);
#ifdef BLUETOOTH_DEBUG
        Serial1.println("Triggered: R ");
        Serial1.println(ail);

#endif
#ifdef DEBUG
        Serial.println("Triggered: R ");
        Serial1.println(ail);
#endif

      }            
      else if(seek_val == SEEK_OPTIMAL)
      {
        Serial.println("Reach");
        ail = mid_rec;

      }


    }
    /*
    else
    {
      ail = mid_rec;

      //AIL.writeMicroseconds(ail);
    }
    */
    
  }
    
}




