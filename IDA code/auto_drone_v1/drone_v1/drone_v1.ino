
/**
Date: 13 Sept 2014
IDA Lab


*/

#include <Servo.h>


#include "drone_ping.h"
#define LED 13

#define STICK_DISTANCE 70 //was 30
/**/


#define DEBUG x

Servo AIL;
Servo ELE;
Servo THR;
Servo RUD;
Servo AUX;

double InputZ, InputR, InputL, hoverHeight, latDiff, ratio, error;
int recAIL;
int recELE;
int recTHR;
int recRUD;
int recAUX;

int max_rec;
int min_rec;
int mid_rec;
int ref_aux;
int thr, ail;

unsigned long x_prev_time;
unsigned long y_prev_time;
unsigned long x_curr_time;
unsigned long x_elap_time;
unsigned long y_elap_time;
unsigned long y_curr_time;

int counter;

void setup(){

  // set up serial baud rate 
  Serial.begin(9600);

  //set up the pins for controls and inputs
  pinMode(trigZ, OUTPUT);
  pinMode(echoZ, INPUT);
  pinMode(trigR, OUTPUT);
  pinMode(echoR, INPUT);
  pinMode(trigL, OUTPUT);
  pinMode(echoL, INPUT);
  pinMode(trigF, OUTPUT);
  pinMode(echoF, INPUT);
  pinMode(trigB, OUTPUT);
  pinMode(echoB, INPUT);

  //set up the pins for flight controller
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  
  //attaching servos
  AIL.attach(9);
  ELE.attach(10);
  THR.attach(11);
  RUD.attach(12);

  //calibrate input and find the average value after taking 10 samples
  recAIL = pulseIn(3, HIGH, 20000);
  for (int i = 0; i < 10; i++) {
    mid_rec += recAIL;
  }
  ref_aux = pulseIn(7, HIGH, 20000);
  mid_rec = mid_rec / 10;//average
  min_rec = mid_rec - 450;//minimum value
  max_rec = mid_rec + 450;//maximum value
  
  Serial.print("AIL AVG VAL: ");
  Serial.println(mid_rec);
  Serial.print("AUX AVG VAL: ");
  Serial.println(ref_aux);
  x_prev_time = millis();
  delay(500);

  hoverHeight=60;//target hover height that we want the unit to go

  fakePulse();
  delay(500);

}

void loop(){

  //reading recievers value from remote control
  recAIL = pulseIn(3, HIGH, 20000);
  recELE = pulseIn(4, HIGH, 20000);
  recTHR = pulseIn(5, HIGH, 20000);
  recRUD = pulseIn(6, HIGH, 20000);
  recAUX = pulseIn(7, HIGH, 20000);

  //check if aux switch is flipped. if so trigger auto pilot
  if(recAUX > 0 && recAUX < (ref_aux - 400)  ){

      digitalWrite(LED,HIGH);
      autoPilot();
    }
    else{
      //else, pass remote control signals to flight controller
      digitalWrite(LED, LOW);
      THR.writeMicroseconds(recTHR);
    }
    // pass ail, ele, rud signal values from remote control to flight controller
    AIL.writeMicroseconds(recAIL);
    ELE.writeMicroseconds(recELE);
    RUD.writeMicroseconds(recRUD);

}

void fakePulse(){
  AIL.writeMicroseconds(1500); 
  delay(10);
  ELE.writeMicroseconds(1500); 
  delay(10);
  THR.writeMicroseconds(1100); 
  delay(10);
  RUD.writeMicroseconds(1500); 
  delay(10);
}


void autoPilot(){
 
  flight_z(); //apply hover algorithmn

}

void flight_z(){

  // if user intervenes using Throttle, the values from the remote controll will overright the autopilot.
  if(recTHR > min_rec + STICK_DISTANCE){
    THR.writeMicroseconds(recTHR);
  }
  else{
    //measure height
    InputZ = pingZ();
    
    if( InputZ > 180){
      thr = min_rec;
    }
    else if( InputZ < hoverHeight - (hoverHeight * 0.20) ){ //below hoverHeight
      error = ((hoverHeight - InputZ)/hoverHeight) * 100;
      thr = map(error, 0, 100, mid_rec, (mid_rec + 200));
    }
    else if( ( InputZ > hoverHeight + (hoverHeight * 0.20) ) && thr >= (mid_rec - 100) ){ // above hoverHeight
      thr-= 6;
    }

    else if( InputZ > hoverHeight - (hoverHeight * 0.20) && InputZ < hoverHeight + (hoverHeight * 0.20) ) { //within hoverHeight
        thr = mid_rec - 100;
    }

    //pass the computed throttle power to the flight controller
    THR.writeMicroseconds(thr);

  }

}




