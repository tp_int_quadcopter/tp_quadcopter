#include <Servo.h>
#define LED 13

Servo AIL;
Servo ELE;
Servo THR;
Servo RUD;
Servo AUX;


int recAIL;
int recELE;
int recTHR;
int recRUD;
int recAUX;


void setup() {

  Serial.begin(9600);
  Serial1.begin(9600);
  Serial1.println("BT Connected");
  pinMode(8, INPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  AIL.attach(9);
  ELE.attach(10);
  THR.attach(11);
  RUD.attach(12);

  digitalWrite(8, HIGH);
  fakePulse();

  delay(500);
}

void loop() {

  recAIL = pulseIn(3, HIGH, 20000);
  recELE = pulseIn(4, HIGH, 20000);
  recTHR = pulseIn(5, HIGH, 20000);
  recRUD = pulseIn(6, HIGH, 20000);
  recAUX = pulseIn(7, HIGH, 20000);
  //Serial.println(recAUX);


  AIL.writeMicroseconds(recAIL);
  ELE.writeMicroseconds(recELE);
  THR.writeMicroseconds(recTHR);
  RUD.writeMicroseconds(recRUD);

  Serial.print("AIL: ");
  Serial.print(recAIL);
  Serial.print('\t');
  Serial.print("ELE: ");
  Serial.print(recELE);
  Serial.print('\t');
  Serial.print("THR: ");
  Serial.print(recTHR);
  Serial.print('\t');
  Serial.print("RUD: ");
  Serial.print(recRUD);
  Serial.print('\t');
  Serial.print("AUX: ");
  Serial.print(recAUX);
  Serial.print('\n');
  //delay(500);
  //Serial.println(recAUX);
  // flight_x();
}

void fakePulse() {

  AIL.writeMicroseconds(1500);
  delay(10);
  ELE.writeMicroseconds(1500);
  delay(10);
  THR.writeMicroseconds(1100);
  delay(10);
  RUD.writeMicroseconds(1500);
  delay(10);

}






