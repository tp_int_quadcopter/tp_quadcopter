void GetPosition() {
//  dist_F = Ping(PIN_TRIGF, PIN_ECHOF); //Get space in front
//  dist_R = Ping(PIN_TRIGR, PIN_ECHOR); //Get space at right
   dist_Z_cm = Ping(PIN_TRIGZ, PIN_ECHOZ); //Get space below
//  dist_L = Ping(PIN_TRIGL, PIN_ECHOL); //Get space at left
}

void ReadReceiver() {
 in_rec_ail = pulseIn(PIN_AIL_IN, HIGH, REC_TIMEOUT_US); //Read aileron
 in_rec_ele = pulseIn(PIN_ELE_IN, HIGH, REC_TIMEOUT_US); //Read elevator
 in_rec_thr = pulseIn(PIN_THR_IN, HIGH, REC_TIMEOUT_US); //Read throttle
 in_rec_rud = pulseIn(PIN_RUD_IN, HIGH, REC_TIMEOUT_US); //Read Rudder
 in_rec_aux = pulseIn(PIN_AUX_IN, HIGH, REC_TIMEOUT_US); //Read Auxiliary
}

float Ping(int ping_pin, int echo_pin) {
  volatile float distance; //Distance of object
  volatile long echo_time; //Time for pulse to return
  
  digitalWrite(ping_pin, LOW); //Ensure clean Pulse
  delayMicroseconds(2);
  digitalWrite(ping_pin, HIGH); //Pulse for 10 microsecond
  delayMicroseconds(10);
  digitalWrite(ping_pin, LOW); //Turn off again
  
  echo_time = pulseIn(echo_pin, HIGH, TRIG_TIMEOUT_US); //Read pulse width
  distance = CalDist(echo_time); //Calculate Distance from time
  if(echo_time == 0) {
    distance = OUT_OF_RANGE_CM;
  }
  return distance;
}

float CalDist(int time) {
  return ((time/2) / 29.1); // t/2 for one trip
}
