
#ifndef IDA_DRONE_PING_INCLUDED
#define IDA_DRONE_PING_INCLUDED

#include <Servo.h>
#include <Arduino.h>



#define trigZ 40
#define echoZ 30

#define trigR 38
#define echoR 28

#define trigL 36
#define echoL 26

#define trigF 34
#define echoF 24

#define trigB 32
#define echoB 22

int pingZ();
int pingR();
int pingL();
int pingF();
int pingB();

unsigned long xcheck_distance(int , int );
int check_distance(int , int );
#endif

