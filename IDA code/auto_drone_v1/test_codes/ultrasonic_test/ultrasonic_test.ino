#define trigZ 40
#define echoZ 30

#define trigR 38
#define echoR 28

#define trigL 36
#define echoL 26

#define trigF 34
#define echoF 24

#define trigB 32
#define echoB 22


void setup() {
  Serial.begin(9600);
  pinMode(trigZ, OUTPUT);
  pinMode(echoZ, INPUT);
  pinMode(trigR, OUTPUT);
  pinMode(echoR, INPUT);
  pinMode(trigL, OUTPUT);
  pinMode(echoL, INPUT);
  pinMode(trigF, OUTPUT);
  pinMode(echoF, INPUT);
  pinMode(trigB, OUTPUT);
  pinMode(echoB, INPUT);


}
void loop() {
  Serial.print("Z: ");
  Serial.print(pingZ());
  Serial.print("\t");
  
  Serial.print("L: ");
  Serial.print(pingL());
  Serial.print("\t");
  
  Serial.print("R: ");
  Serial.print(pingR());
  Serial.print("\t");
  
  Serial.print("F: ");
  Serial.print(pingF());
  Serial.print("\t");
  
  Serial.print("B: ");
  Serial.print(pingB());
  Serial.print("\t");
  
  Serial.print("\n");

delay(10);  
}

int pingZ() {
  check_distance(trigZ, echoZ);
}
int pingR() {
  check_distance(trigR, echoR);
}
int pingL() {
  check_distance(trigL, echoL);
}

int pingF() {
  check_distance(trigF, echoF);
}

int pingB() {
  check_distance(trigB, echoB);
}

int check_distance(int t, int e) {
  long duration, distance;
  digitalWrite(t, LOW);  
  delayMicroseconds(2); 
  digitalWrite(t, HIGH);
  delayMicroseconds(10); 
  digitalWrite(t, LOW);
  duration = pulseIn(e, HIGH);
  distance = (duration / 2) / 29.1;
  return (distance);
}


