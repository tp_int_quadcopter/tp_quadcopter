#ifndef IDA_DRONE_FLY_INCLUDED
#define IDA_DRONE_FLY_INCLUDED


#define IDA_FRONT 10
#define IDA_BACK 20
#define IDA_LEFT 30
#define IDA_RIGHT 40
#define IDA_NEUTRAL 0

#define AIELERON_MAX_R_SEEK_OUT 50
#define AIELERON_MID_R_SEEK_OUT 150
#define AIELERON_SLIGHT_R_SEEK_OUT 170

#define POWER_R_MAX 30
#define POWER_R_MID 30
#define POWER_R_LOW 30
#define POWER_L_REG 30

#define WALL_TOLERANCE_MIN 180
#define WALL_TOLERANCE_MAX 150

#define SPOOKY_RANGE 10


#define SEEK_OPTIMAL 3

#define SEEKING_IN 0
#define SEEKING_OUT 1



#define TOO_CLOSE 100
#define SOMEWHAT_CLOSE 120
#define NOT_CLOSE 150




#define MAX_SEEK_TICKS 100 //the maximum time to seek before shutting down



void hello();
int seek(int );

#endif
